# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

### Mise en place de la conteneurisation du projet
# 1- Créer le fichier Dockerfile
    - 1ere partie du Dockerfile (lignes 1 a 7) va permettre de gérer le package 
    - 2eme partie du Dockerfile (lignes 9 a 13) va permettre de ne récupérer que le .jar généré dans la 1ere partie

# 2- Créer l'image contenant le .jar grace au Dockerfile (build)
    dans un terminal : 
    docker image build -t simple-http-hello-world .

# 3- Lancer le conteneur (run)
    dans un terminal : 
    docker run -d -p 8080:8080 simple-http-hello-world


### Mise en place d'une pipeline d'intégration continue pour ce projet 
# 1- Créer un fichier .gitlab-ci.yml
    - va permettre de créer le package (build + package), et l'image sera disponible dans le registry container (store)

# 2- Lancer la pipeline
    dans un terminal
    - sauver le projet dans gitlab et le push pour démarrer la pipeline
    git add .
    git status
    git commit -m "pipeline integration continue"
    git push

# 3- Voir le déroulement de la pipeline dans gitlab
    sur gitlab : dans CI/CD, pipelines, on peut cliquer sur status afin de suivre la progression

# 4- Verifier que l'image est disponible dans le registry container
    sur gitlab : dans Packages & Registries, Container registry
    